const fs = require("fs"); // untuk membaca file, mengkonversi ke stream
const readline = require("readline"); // digunakan untuk membaca file line by line
const http = require("http");

const PORT = 3088;
const lineReader = readline.createInterface({
  input: fs.createReadStream('./README.md') // baca file 
});

// gunakan regex untuk mencocokan token Markdown
// regex dibawah adalah regex untuk link tag contoh: [ini](https://adalah.link)
const linkTagRegex = /\[([^\]]+)\][^\)]+\)/g;
const linkRegexHashtag = /#/g;
const linkRegexStrip = /-|PS:/g;

let HTML = "";

// baca dan proses file per baris
lineReader
  .on("line", function (line) {
    let parse = parseLine(line);
    for (var i = 0; i < parse.length; i++) {
      HTML += parse[i];
    }
  })
  .on("close", function () {
    // setelah selesai memparse semua line markdown,
    // serve sebagai HTML
    const result = generateValidHTML(HTML);
    http
      .createServer((request, response) => {
        response.writeHead(200, {});
        response.write(result);
        response.end();
      })
      .listen(PORT, () => console.log(`Server listen at port ${PORT}`));
  });

// fungsi ini berguna untuk merubah setiap line yang
// masuk menjadi file HTML
// untuk melihat beberapa aturan markdown bisa gunakan ini https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
// ingat buatlah dengan hasil semirip mungkin dengan https://maxsol-test-parser.glitch.me

function parseLine(line) {
  let htmlText = '';
  let checkHashtag = line.match(linkRegexHashtag);
  let checkStrip = line.match(linkRegexStrip);
  let hastagLen = checkHashtag === null ? 0 : checkHashtag.length;
  let stripLen = checkStrip === null ? 0 : checkStrip.length;
  let checkLink = line.match(linkTagRegex);
  let checkLinkLen = checkLink === null ? 0 : checkLink.length;
  
  if (hastagLen == 1) {
    htmlText = `<h1>${line.replace(/#\s/g, "")}</h1>`;
  } else if (hastagLen == 2) {
    htmlText = `<h2>${line.replace(/##\s/g, "")}</h2>`;
  } else if (stripLen == 1) {
    htmlText = `<li>${line.replace(/-\s/g, "")}</li>`;
  } else {
    if (line.length < 1) {
      htmlText = ``;
    } else {
      let boldItalic = /\_([^\]]+)\_|\*([^\]]+)\*/g; // mencari character mengandung **bold text**
      let checkBoldItalic = line.match(boldItalic);
      let boldItalicLn = checkBoldItalic === null ? 0 : checkBoldItalic.length;

      if (checkLinkLen > 0) {
        let stpA = /[\[\]]/g; // Get [ and ]
        let stpB = /[\(\)]/g; // Get ( and )
        let stpD = /(\[([^\]]+)\]|\(([^\]]+)\))/g; // [seperti ini] and (https://google.com)
        let stpF = line.match(stpD);
        let aContent = stpF[0].replace(stpA, "");
        let hrefContent = stpF[1].replace(stpB, "");
        let linkTagRegex = /\[([^\]]+)\][^\)]+\)/g;
        let aHref = line.replace(linkTagRegex, `<a href="${hrefContent}">${aContent}</a>`);
        aHref = aHref.replace(/PS:\s/g, "");
        htmlText = `<li>${aHref}</li>`;
      } else if (boldItalicLn > 0) {
        let boldContent = /\*/g;
        let italicContent = /\_/g;
        let replaceBold = checkBoldItalic[0].replace(boldContent, "");
        let replaceItalic = checkBoldItalic[1].replace(italicContent, "");
        let obj = {
          '**bold text**': `<b>${replaceBold}</b>`,
          '_italic_': `<i>${replaceItalic}</i>`
        }

        let str = '';
        str = line.replace(boldItalic, function(match) {
          return obj[match]
        })
        htmlText = `<p>${str}</p>`;
      } else {
        htmlText = `<p>${line}</p>`;
      }
    }
  }

  return htmlText;
}
// beberapa aturan & tips
// - gunakan regex
// - jangan gunakan library parser :'( ini hanya test saja, silahkan manfaatkan core library node.js dan javascript!
// - Make it work then make it beautifull
// - Semangat! jikalau ada yang ditanyakan bisa langsung kontak Team Maxsol.id

function generateValidHTML(html) {
  return `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8" />
          <title> simple markdown parser </title>
        <head>
      <body style="max-width: 600px; line-height: 1.5; margin: auto;">
        ${html}
      </body>
      </html>
    `;
}

// THANKS, created by - mandaputtra
